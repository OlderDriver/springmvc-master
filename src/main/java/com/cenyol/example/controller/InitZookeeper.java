package com.cenyol.example.controller;

import java.io.InputStream;
import java.util.Properties;

import org.apache.zookeeper.ZooKeeper;

public enum InitZookeeper {
    INSTANCE;
    private static ZooKeeper zk;
    static {
        try {
        	 Properties pro = new Properties();  
        	 InputStream in = InitZookeeper.class.getResourceAsStream("/config.properties");
        	 pro.load(in);
        	 String ip = pro.getProperty("ip");
        	 in.close();
        	 ZooKeeperConn zkconn = new ZooKeeperConn(ip);
        	 zkconn.connect();
        	 zk = zkconn.getZk();
        	 
        	/*zk = new ZooKeeper(ip, 5000, new Watcher() {
				
				@Override
				public void process(WatchedEvent arg0) {
					// TODO Auto-generated method stub
					System.out.println("create zk");
				}*/
        } catch (Exception e) {
        	e.printStackTrace();
        }

    }

    public ZooKeeper getZooKeeper(){

        return zk;
    }

}

