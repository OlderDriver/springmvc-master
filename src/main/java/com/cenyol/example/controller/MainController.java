package com.cenyol.example.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletResponse;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooKeeper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cenyol.example.pojo.Node;



/**
 * 
 */
@Controller
public class MainController {

	ZooKeeper zk = InitZookeeper.INSTANCE.getZooKeeper();

	// 首页
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) throws IOException, KeeperException, InterruptedException {

		List<String> children = new ArrayList<>();
		Properties pro = new Properties();
		InputStream in = InitZookeeper.class.getResourceAsStream("/list.properties");
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																						pro.load(in);
		String nodeList = pro.getProperty("nodeList");
		String[] strings = nodeList.split(",");
		for (String string : strings) {
			children.add(string);
		}
		in.close();
		List<Node> list = new ArrayList<>();
		for (String string : children) {
			Node node = new Node();
			node.setZkPath("/" + string);
			// node.setZkData(new String(zk.getData(path+string, true, null)));
			byte[] data = zk.getData("/" + string, true, null);
			if (data != null) {
				node.setZkData(new String(data));
			}

			list.add(node);
			List<Node> list2 = this.queryList("/" + string);
			if (list2 == null) {
				continue;
			}
			for (Node node2 : list2) {
				list.add(node2);
			}
		}

		model.addAttribute("list", list);
		return "index";
	}

	public List<Node> queryList(String zkPath) throws KeeperException, InterruptedException, IOException {
		List<String> list = zk.getChildren(zkPath, true);
		List<Node> list2 = new ArrayList<>();
		for (String string : list) {
			Node node = new Node();
			node.setZkPath(zkPath + "/" + string);
			byte[] data = zk.getData(zkPath + "/" + string, true, null);
			if (data != null) {
				node.setZkData(new String(data));
			}
			list2.add(node);
			List<Node> list3 = queryList(zkPath + "/" + string);
			if (list3 == null) {
				continue;
			}
			for (Node node2 : list3) {
				list2.add(node2);
			}
		}
		return list2;
	}

	/**
	 * 首页向详情页跳转方法
	 * 
	 * @param zkPath
	 * @param model
	 * @return
	 * @throws IOException
	 * @throws KeeperException
	 * @throws InterruptedException
	 */
	@RequestMapping(value = "/ToDetailPage")
	public String toPage(@RequestParam("zkPath") String zkPath, Model model)
			throws IOException, KeeperException, InterruptedException {

		if (zkPath != null && zkPath != "") {

			byte[] data = zk.getData(zkPath, true, null);
			Node node = new Node();
			if (data != null) {
				node.setZkData(new String(data));
			}
			node.setZkPath(zkPath);
			model.addAttribute("bean", node);

		}
		return "nodeDetail";
	}

	/**
	 * 根据path查询数据转化为json格式
	 * 
	 * @param zkPath
	 * @param model
	 * @return
	 * @throws IOException
	 * @throws KeeperException
	 * @throws InterruptedException
	 */
	@RequestMapping(value = "/queryData")
	public void queryData(@RequestParam("zkPath") String zkPath, Model model, HttpServletResponse resp)
			throws IOException, KeeperException, InterruptedException {

		if (zkPath != null && zkPath != "") {

			byte[] data = zk.getData(zkPath, true, null);
			if (data != null) {
				String[] split = new String(data).split(",");

				if (data != null) {
					List<Map<String, Object>> list = new ArrayList<>();
					for (String string : split) {
						Map<String, Object> dataMap = new HashMap<>();
						dataMap.put("dataDetail", string);
						list.add(dataMap);
					}
					String jsonString = JSON.toJSONString(list);
					resp.getWriter().write(jsonString);
				}
			}
		}
	}

	// 添加、更新节点
	@SuppressWarnings("unused")
	@RequestMapping(value = "/update")
	@ResponseBody
	public void addNode(@RequestParam("zkData") String zkData, @RequestParam("zkPath") String zkPath,
			HttpServletResponse resp) throws IOException, KeeperException, InterruptedException {

		try {
			String updateData = "";
			JSONArray parseArray = JSON.parseArray(zkData);
			for (int i = 0; i < parseArray.size(); i++) {
				JSONObject jsonObject = parseArray.getJSONObject(i);
				String string = jsonObject.getString("dataDetail");
				if (string == "" && string == null) {
					continue;
				}
				if (i == parseArray.size() - 1) {
					updateData = updateData + string;
				} else {
					updateData = updateData + string + ",";
				}
			}
			if (zk.exists(zkPath, true) == null) {
				if (updateData == "") {
					write(resp, ajaxReturn(false, "数据不能为空"));
				}else {
					zk.create(zkPath, updateData.getBytes(), Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
					write(resp, ajaxReturn(true, "操作成功"));
				}
			} else {
				if (updateData == "") {
					write(resp, ajaxReturn(false, "数据不能为空"));
				}else {
					zk.setData(zkPath, updateData.getBytes(), -1);
					write(resp, ajaxReturn(true, "操作成功"));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			write(resp, ajaxReturn(false, "操作失败"));
		}
	}

	// 删除节点
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public String deleteNode(Node node, HttpServletResponse resp)
			throws IOException, KeeperException, InterruptedException {
		try {
			zk.delete(node.getZkPath(), -1);

			return "success";
		} catch (Exception e) {
			e.printStackTrace();
			return "false";
		}

	}

	public void write(HttpServletResponse resp,String jsonString) {

		resp.setCharacterEncoding("UTF-8");
		try {
			resp.getWriter().print(jsonString);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String ajaxReturn(boolean success, String message) {

		Map map = new HashMap();
		map.put("success", success);
		map.put("message", message);
		return JSON.toJSONString(map);
	}

	/*
	 * //修改节点
	 * 
	 * @RequestMapping(value = "/update", method = RequestMethod.GET)
	 * 
	 * @ResponseBody public void updateNode(Node node) throws IOException,
	 * KeeperException, InterruptedException{
	 * 
	 * ZooKeeper zk = InitZooKeeper();
	 * 
	 * zk.setData(node.getPath(), node.getData().getBytes(), -1);
	 * 
	 * zk.close(); }
	 */
	// 查询节点
	/*
	 * @RequestMapping(value = "/query", method = RequestMethod.GET) public void
	 * queryNode(String path,Model model) throws IOException, KeeperException,
	 * InterruptedException{
	 * 
	 * ZooKeeper zk = InitZooKeeper();
	 * 
	 * List list = (List) zk.getChildren(path, true);
	 * 
	 * model.addAttribute("list", list); }
	 */
	/*
	 * @RequestMapping(method = RequestMethod.GET)
	 * 
	 * @ResponseBody public List
	 * queryContentCategoryByParentId(@RequestParam(value = "id", defaultValue =
	 * "/") String path) throws IOException, KeeperException,
	 * InterruptedException { ZooKeeper zk = InitZooKeeper(); List<String> list
	 * = zk.getChildren(path, true); List<Node> list2 = new ArrayList<Node>();
	 * for (String string : list) { Node node = new Node();
	 * node.setPath(string); list2.add(node); } return list2; }
	 */

}