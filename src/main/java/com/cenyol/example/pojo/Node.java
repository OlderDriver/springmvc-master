package com.cenyol.example.pojo;

public class Node {

	private int id;
	private int parentId;
	private String zkPath;
	private int isParent;
	private String zkData;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public String getZkPath() {
		return zkPath;
	}
	public void setZkPath(String zkPath) {
		this.zkPath = zkPath;
	}
	public String getZkData() {
		return zkData;
	}
	public void setZkData(String zkData) {
		this.zkData = zkData;
	}
	public int getIsParent() {
		return isParent;
	}
	public void setIsParent(int isParent) {
		this.isParent = isParent;
	}
		
}
