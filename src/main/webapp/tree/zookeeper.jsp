<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div>
	 <ul id="contentCategory" class="easyui-tree">
    </ul>
</div>
<div id="contentCategoryMenu" class="easyui-menu" style="width:120px;" data-options="onClick:menuHandler">
    <div data-options="iconCls:'icon-add',name:'add'">添加</div>
    <div data-options="iconCls:'icon-remove',name:'rename'">重命名</div>
    <div class="menu-sep"></div>
    <div data-options="iconCls:'icon-remove',name:'delete'">删除</div>
</div>
<script type="text/javascript">
//当页面加载成功后执行以下逻辑
$(function(){
	//id选择器，其实获取到的是树组件
	$("#contentCategory").tree({
		//创建树发起的请求
		url : '/query',
		animate: true,
		method : "GET",
		//点击鼠标右键，执行以下逻辑
		onContextMenu: function(e,node){
			//关闭鼠标右键的点击事件，其实就是取消系统默认的右键菜单
            e.preventDefault();
			//选中选择的节点
            $(this).tree('select',node.target);
			//获取菜单组件，执行show方法，其实就是把菜单显示出来了
            $('#contentCategoryMenu').menu('show',{
            	//显示菜单的参数，其实就是设置的鼠标所在的位置
                left: e.pageX,
                top: e.pageY
            });
        },
        
        //在编辑之后执行以下逻辑
        onAfterEdit : function(node){
        	var _tree = $(this);
        	//判断节点的id是否为0，实际是判断节点是否是新增的
        	if(node.id == 0){
        		// 新增节点
        		$.post("/add",{parentId:node.parentId,name:node.text},function(data){
        			//树执行更新方法
        			_tree.tree("update",{
        				//新增的节点进行更新
        				target : node.target,
        				//更新的数据，这里只是更新id，更新的值请求后台返回的数据
        				id : data.id
        			});
        		});
        	//如果id不为0，表示这里要坐修改节点的操作
        	}else{
        		$.ajax({
        			   type: "POST",
        			   url: "/update",
        			   data: {id:node.id,name:node.text},
        			   success: function(msg){
        				   //$.messager.alert('提示','新增成功!');
        			   },
        			   error: function(){
        				   $.messager.alert('提示','操作失败!');
        			   }
        			});
        	}
        }
	});
});

//菜单组件绑定的点击事件，参数item就是点击的那个项目
function menuHandler(item){
	//获取树
	var tree = $("#contentCategory");
	//执行树的getSelected方法，目的就是获取选择的节点
	var node = tree.tree("getSelected");
	//===表示比较数据的内容和类型。==表示比较数据的内容不比较类型
	//alert(1===1);
	//alert(1==="1");
	//alert(1==1);
	//alert(1=="1");
	//判断点击的菜单是属于添加按钮
	if(item.name === "add"){
		//执行树的append方法，追加节点
		tree.tree('append', {
			//设置新增节点的父
            parent: (node?node.target:null),
            //设置新增节点的数据
            data: [{
                text: '新建分类',
                id : 0,
                parentId : node.id
            }]
        }); 
		//在树上去找id为0的节点，其实就是新增的节点
		var _node = tree.tree('find',0);
		//tree.tree("select",_node.target):选中新增的节点
		//tree('beginEdit',_node.target):开始编辑
		tree.tree("select",_node.target).tree('beginEdit',_node.target);
	//判断点击的菜单是属于重命名按钮
	}else if(item.name === "rename"){
		//对选中的节点开始编辑
		tree.tree('beginEdit',node.target);
	//判断点击的菜单是属于删除按钮
	}else if(item.name === "delete"){
		//提示用户，是否进行删除。删除是危险操作，删除之前一定要用户确认
		$.messager.confirm('确认','确定删除名为 '+node.text+' 的分类吗？',function(r){
			//如果用户确认 ，执行以下逻辑进行删除
			if(r){
				$.ajax({
     			   type: "POST",
     			   url: "/rest/content/category/delete",
     			   data : {parentId:node.parentId,id:node.id},
     			   success: function(msg){
     				   //$.messager.alert('提示','新增商品成功!');
     				   //执行tree的remove方法，删除选中的几点
     				  tree.tree("remove",node.target);
     			   },
     			   error: function(){
     				   $.messager.alert('提示','删除失败!');
     			   }
     			});
			}
		});
	}
}
</script>