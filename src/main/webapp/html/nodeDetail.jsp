<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="zookeeper" content="text/html; charset=UTF-8">
<title>zookeeper后台管理</title>
<script src="https://cdn.bootcss.com/jquery/2.2.0/jquery.js"></script>
<link rel="stylesheet" type="text/css"
	href="ui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="ui/themes/icon.css">
<script type="text/javascript" src="ui/jquery.min.js"></script>
<script type="text/javascript" src="ui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="ui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="ui/jquery.serializejson.min.js"></script>
</head>
<body>
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
		<h1>节点详情</h1>
		</section>


		<section class="content"> <!-- 更新区间begin -->
		<div id="tzqj">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<!-- /.box-header -->
						<div class="box-body">

							<div class="form-group">
								<label>节点路径</label> <input type="text" name="zkPath"
									value="${bean.zkPath}" class="form-control"
									placeholder="请输入节点路径" maxlength="50" style="width: 500px"
									required maxlength='50'>
							</div>
							<form id="form" method="post">
								<div class="form-group">
									<label>节点数据</label>
									<%--  <input type="text" name="zkData"
										value="${bean.zkData}" class="form-control"
										placeholder="请输入节点地址" maxlength="50" style="width: 500px"
										required maxlength='50'> --%>
									<table id="grid"></table>
								</div>


							</form>
							<div>
								<button id="saveBtn">保存</button>
								<button id="exitBtn">返回</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 更新区间end --> </section>
		<!-- /.col -->
	</div>
	<!-- /.row -->


	</div>
</body>
<script type="text/javascript">
	var obj = window.location;
	var contextPath = obj.pathname.split("/")[1];
	var basePath = obj.protocol + "//" + obj.host + "/" + contextPath;
	console.log(basePath)
	$(function() {
		$("#exitBtn").click(function() {
			location.href = basePath;
		});
	});

	var isEditingIndex; //当前编辑行的索引号
	$(function() {
		$("#grid").datagrid({
			url : basePath + "/queryData?zkPath=${bean.zkPath}",
			columns : [ [ {
				field : 'dataDetail',
				title : '数据',
				width : 100,
				editor : {
					type : 'text',
					options : {}
				}
			}, {
				field : '-',
				title : '操作',
				width : 200,
				formatter : function(value, row, index) {
					return "<a href='#' onclick='dele(" + index + ")'>删除</a>";
				}
			} ] ],
			singleSelect : true,
			toolbar : [ {
				text : '添加',
				iconCls : 'icon-add',
				handler : function() {
					$("#grid").datagrid('endEdit', isEditingIndex);//添加新行之前把之前的可编辑行关闭
					$("#grid").datagrid('appendRow', {}); //添加新行
					isEditingIndex = $("#grid").datagrid("getRows").length - 1;//把新的数据放到了表格的最后，拿到表达最后那一行的索引号
					$("#grid").datagrid('beginEdit', isEditingIndex); //把最后一行开启可编辑状态
					bindEvent();
				}
			} ],
			onClickRow : function(index, row) {
				$("#grid").datagrid('beginEdit', index); //把点击的那一行做成可编辑状态
				$("#grid").datagrid('endEdit', isEditingIndex); //要关闭之前的那个一行的可编辑状态
				isEditingIndex = index; //因为现在单击的那一行是可编辑状态，所以把这一行的索引号赋给isEditingIndex
			}
		})

		$("#saveBtn").bind("click", function() {
			//提交数据之前关闭编辑状态 目的就是为了把编辑框中的数据提交给表格
			$("#grid").datagrid("endEdit", isEditingIndex);
			var formData = $("#editForm").serializeJSON();
			//			{"t.supplieruuid":1}
			var rowDatas = $("#grid").datagrid("getRows")
			JSON.stringify(rowDatas);
			formData['jsonStr'] = JSON.stringify(rowDatas);
			//			{"t.supplieruuid":1,"jsonStr":}
			$.ajax({
				url : basePath + '/update?zkPath=${bean.zkPath}',
				data : "zkData=" + JSON.stringify(rowDatas),
				type : 'post',
				dataType : 'json',
				success : function(value) {
					if(value.success){
						$('#grid').datagrid('reload');
					}
					$.messager.alert('提示',value.message);
				}
			})

		})
	})

	/**
	 * 动态删除行
	 */
	function dele(index) {
		//		删除之前要把编辑状态的那一行数据提交给datagrid表格
		$("#grid").datagrid("endEdit", isEditingIndex);
		$("#grid").datagrid("deleteRow", index);
		//		删除数据后 获取一下整个表格中的数据
		var gridDatas = $("#grid").datagrid("getData");
		//		var gridDatas = $("#grid").datagrid("getRows");
		//		alert(JSON.stringify(gridDatas))
		//		把刚刚获取的数据重新加载到表格中
		$("#grid").datagrid("loadData", gridDatas);
	}
</script>
</html>