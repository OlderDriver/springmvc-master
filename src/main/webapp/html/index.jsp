<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="zookeeper" content="text/html; charset=UTF-8">
<title>zookeeper后台管理</title>
<script src="https://cdn.bootcss.com/jquery/2.2.0/jquery.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.js"></script>
<!-- Latest compiled and minified Locales -->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/locale/bootstrap-table-zh-CN.min.js"></script>
</head>
<body>
	<table id="example" data-toggle="table">
		<thead>
			<tr>
				<th>节点路径</th>
				<th>操作</th>

			</tr>
		</thead>
		<tbody>
			<c:forEach var="bean" items="${list}">
				<tr>
					<td>${bean.zkPath}</td>
					<td align="center"><a
						href="javascript:deleteNode('${bean.zkPath}')">删除</a>
						&nbsp;&nbsp; <a
						href="${base}/example/ToDetailPage?zkPath=${bean.zkPath}">查看数据</a>
						&nbsp;&nbsp; <a
						href="${base}/example/ToDetailPage?zkPath=${bean.zkPath}">新增</a>
					</td>
			</c:forEach>
		</tbody>
	</table>
</body>
<script type="text/javascript">
	var obj = window.location;
	var contextPath = obj.pathname.split("/")[1];
	var basePath = obj.protocol + "//" + obj.host + "/" + contextPath;
	function deleteNode(obj) {
		if (confirm("确认删除该节点")) {
			$.ajax({
				type : 'post',
				url : basePath + "/delete",
				data : {
					zkPath : obj
				},
				success : function(data) {
					alert(data);
					if(data=="success"){
						location.href = basePath;
					}
				}
			});
		}
	}
</script>
</html>